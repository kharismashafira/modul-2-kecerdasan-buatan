var nasabah = [
{
	Jenis_Kelamin : "Wanita",
	Pendidikan : 3,
	Bidang_Pekerjaan : 1,
	Kelompok_Usia : 3
},
{
	Jenis_Kelamin : "Pria",
	Pendidikan : 2,
	Bidang_Pekerjaan : 2,
	Kelompok_Usia : 2
},
{
	Jenis_Kelamin : "Wanita",
	Pendidikan : 1,
	Bidang_Pekerjaan : 3,
	Kelompok_Usia : 1
},
{
	Jenis_Kelamin : "Pria",
	Pendidikan : 2,
	Bidang_Pekerjaan : 4,
	Kelompok_Usia : 1
},
{
	Jenis_Kelamin : "Pria",
	Pendidikan : 2,
	Bidang_Pekerjaan : 4,
	Kelompok_Usia : 1
},
{
	Jenis_Kelamin : "Pria",
	Pendidikan : 1,
	Bidang_Pekerjaan : 3,
	Kelompok_Usia : 4
},
{
	Jenis_Kelamin : "Wanita",
	Pendidikan : 1,
	Bidang_Pekerjaan : 2,
	Kelompok_Usia : 3
},
];

var Jenis_Kelamin,Pendidikan,Bidang_Pekerjaan,Kelompok_Usia,submit;
var dataDist = [];

function setup()
{
	createCanvas(500,500);
	background(0);
	
	tampilForm();
}

function tampilForm()
{
	Jenis_Kelamin = createInput();
	Jenis_Kelamin.position(20,100);
	
	Pendidikan = createInput();
	Pendidikan.position(20,130);
	
	Bidang_Pekerjaan = createInput();
	Bidang_Pekerjaan.position(20,160);
	
	Kelompok_Usia = createInput();
	Kelompok_Usia.position(20,190);
	
	submit = createButton('SUBMIT');
	submit.position(20,220);
	submit.mousePressed(inputData);
}

function inputData()
{
	nasabah.push({
		Jenis_Kelamin : Jenis_Kelamin.value(),
		Pendidikan : parseInt(Pendidikan.value()),
		Bidang_Pekerjaan : parseInt(Bidang_Pekerjaan.value()),
		Kelompok_Usia : parseInt(Kelompok_Usia.value())
	});
	console.log("Berhasil Memasukan Nasabah");
	naivebayes();
}

function tampilData()
{
	for( var i = 0 ; i < nasabah.length ; i++)
	{
		console.log(nasabah[i]);
	}
	
	for( var z = 1 ; z < dataDist.length ; z++){
		console.log("Jenis_Kelamin : "+dataDist[z].Jenis_Kelamin+" , Kedekatan : "+dataDist[z].dekat);
	}
	
	console.log("Jadi nasabah "+Jenis_Kelamin.value()+" Mendapat pinjaman ");
}

function naivebayes()
{
	var pBaru = parseInt(Bidang_Pekerjaan.value());
	var rBaru = parseInt(Kelompok_Usia.value());
	
	for( var i = 0 ; i < nasabah.length ; i++)
	{
		var neig = dist(pBaru,rBaru,nasabah[i].Bidang_Pekerjaan,nasabah[i].Kelompok_Usia);
		dataDist.push({
			Jenis_Kelamin : nasabah[i].Jenis_Kelamin,
			dekat : neig,
			jumlah : nasabah[i].Bidang_Pekerjaan
		});
		//console.log(i);
	}
	dataDist.sort(dariJarak);
	
	function dariJarak(a,b)
	{
		return a.dekat - b.dekat;
	}
}